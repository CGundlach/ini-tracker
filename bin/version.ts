// Initiative Tracker
//
// Copyright 2018 Christopher Gundlach

// tslint:disable-next-line:no-var-requires
// const version = require('./../package.json').version;
// tslint:disable-next-line:no-var-requires

import fs = require('fs');
import path = require('path');

const version = JSON.parse(
  fs.readFileSync(path.join('package.json')).toString()
).version;
const changelog = fs.readFileSync(path.join('CHANGELOG')).toString();

fs.writeFileSync(path.join('src', 'assets', 'VERSION'), version);

fs.writeFileSync(path.join('src', 'assets', 'CHANGELOG'), changelog);
