# Copyright (c) 2018 Christopher Gundlach

import ftplib
import sys
import getopt
import os
import stat

# def upload_file(file):


def connectToFTP(host, user, password):
    ftp = ftplib.FTP()
    print('connecting to', host[0], 'on port', host[1])
    try:
        ftp.connect(host[0], int(host[1]))
    except:
        print('Connection failed')
        sys.exit(2)
    print('logging in as', user)
    try:
        ftp.login(user, password)
    except:
        print('Login failed')
        sys.exit(2)
    return ftp


def deleteFolder(ftp, directory):
    for item in ftp.nlst(directory):
        try:
            print('deleting', item)
            ftp.delete(item)
        except ftplib.error_perm:
            deleteFolder(ftp, item)
            ftp.rmd(item)


def uploadFolder(ftp, directory):
    if directory is not '.':
        try:
            print('uploading directory:', directory)
            ftp.mkd(directory)
        except:
            print(sys.exc_info())
            ftp.quit()
            sys.exit(2)
    directory += '/'
    for item in os.listdir(directory):
        item = directory+item
        if stat.S_ISDIR(os.stat(item).st_mode):
            uploadFolder(ftp, item)
        elif stat.S_ISREG(os.stat(item).st_mode):
            file = open(item, 'rb')
            print('uploading file:', item)
            try:
                ftp.storbinary('STOR ' + item, file)
            except:
                print(sys.exc_info())
                ftp.quit()
                sys.exit(2)


def main(argv):
    os.chdir('dist')
    host = ''
    user = ''
    password = ''
    try:
        opts, args = getopt.getopt(argv, "h:u:p:", ["host=", "user=", "pass="])
    except getopt.GetoptError:
        print('usage: deploy.py -h <host:port> -u <user> -p <password>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--host"):
            host = arg.split(':', 1)
        elif opt in ("-u", "--user"):
            user = arg
        elif opt in ("-p", "--pass"):
            password = arg

    if host == '':
        print('usage: deploy.py -h <host:port> -u <user> -p <password>')
        print('no host was given, aborting ...')
        sys.exit(2)
    if user == '':
        print('usage: deploy.py -h <host:port> -u <user> -p <password>')
        print('no user was given, defaulting to anonymous')
    if password == '':
        print('usage: deploy.py -h <host:port> -u <user> -p <password>')
        print('no password was given, defaulting to anonymous@')
    ftpConnection = connectToFTP(host, user, password)
    deleteFolder(ftpConnection, '')
    uploadFolder(ftpConnection, '.')

    ftpConnection.quit()


if __name__ == "__main__":
    main(sys.argv[1:])
