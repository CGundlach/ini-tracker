import { ISystem } from '../app/models/system';

export const SYSTEMS: ISystem[] = [
  {
    name: 'kein System',
    description:
      'Direkte Eingabe der erwürfelten Initiative-Werte. Die rechte Liste wird bei Eingabe direkt sortiert.',
    diceSides: 0,
    diceCount: 0,
    iniCalc: 'none'
  },
  {
    name: 'kein System +Ini-Durchgänge',
    description:
      'Direkte Eingabe der erwürfelten Initiative-Werte. Die rechte Liste wird bei Eingabe direkt sortiert.' +
      ' Mehrere Initiativedurchgänge werden unterstützt',
    diceSides: 0,
    diceCount: 0,
    iniCalc: 'none',
    multipleInitiativePasses: true
  },
  {
    // Arcane Codex ist ein eingetragenes Warenzeichen von Nackter Stahl®, Köln. Copyright by Nackter Stahl® 1999-2018.
    name: 'Arcane Codex',
    description:
      'Es werden zwei W10 gewürfelt und die Summe der Augenzahlen auf den Initiativewert addiert.',
    diceSides: 10,
    diceCount: 2,
    iniCalc: 'sum',
    // http://www.nackterstahl.de/index.php/copyright/
    copyrightNotice:
      'Arcane Codex ist ein eingetragenes Warenzeichen von Nackter Stahl®, Köln. Copyright by Nackter Stahl® 1999-2018.'
  },
  {
    name: 'Erfolge auf W6',
    description:
      'Es werden W6 in Höhe der Initiative geworfen. ' +
      'Die Anzahl der Würfel, die eine 5 oder 6 zeigen wird auf den Initiativewert addiert. ' +
      'Patzer, wenn mindestens die Hälfte aller Würfel eine 1 zeigen. ' +
      'Mehrere Initiativedurchgänge werden unterstützt',
    diceSides: 6,
    diceCount: 'initiative',
    multipleInitiativePasses: true,
    iniCalc: 'countHits',
    glitch: 'halfOnes',
    criticalGlitch: 'noHits'
  }
];

/*
  example:
  {
    name: 'Example System', // displayed in dropdown and title
    description: 'Roll 13d37 and add the result to initiative', // Short description of the initiative calculation process
    diceSides: 13,  // The number of sides of the used dice
    diceCount: 37,  // The amount of dice to roll.
                    // Values available: any number or 'initiative', which sets the amount of dices rolled
                    //  to the initiative of the character
    iniCalc: 'sum', // How the initiative is calculated. Values available: 'none', 'sum', 'countHits'.
                    // 'none' uses the characters initiative to sort the list, 'sum' adds the initiative to the sum of the dice roll result,
                    // 'countHits' counts the number of dice showing a 5 or 6 and adds that to the initiative
    // http://example.com
    copyrightNotice: 'Example system is a made up system. No copyright notice needed'
                    // Copyright notice that is displayed when the system is selected. Include source in the line above as comment
  }
*/
