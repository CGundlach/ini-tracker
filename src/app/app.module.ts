// Copyright (c) 2018 Christopher Gundlach

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { UnitService } from './services/unit-manager.service';

import {
  MatButtonModule,
  MatDialogModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSidenavModule,
  MatTableModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutDialogComponent } from './components/about-dialog/about-dialog.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { CopyrightNoticeComponent } from './components/initracker/copyright-notice/copyright-notice.component';
import { IniListComponent } from './components/initracker/ini-list/ini-list.component';
import { IniTrackerComponent } from './components/initracker/initracker.component';
import { UnitListComponent } from './components/initracker/unit-list/unit-list.component';

@NgModule({
  declarations: [
    AppComponent,
    IniTrackerComponent,
    UnitListComponent,
    IniListComponent,
    CopyrightNoticeComponent,
    FooterComponent,
    HeaderComponent,
    AboutDialogComponent
  ],
  imports: [
    BrowserModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatListModule,
    MatTooltipModule,
    MatSelectModule,
    MatTableModule,
    MatSidenavModule,
    LayoutModule,
    MatToolbarModule,
    MatIconModule,
    AppRoutingModule,
    MatDialogModule,
    HttpClientModule,
    MatExpansionModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [UnitService],
  bootstrap: [AppComponent],
  entryComponents: [AboutDialogComponent]
})
export class AppModule {}
