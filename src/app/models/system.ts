// Copyright (c) 2018 Christopher Gundlach

export interface ISystem {
  name: string;
  description: string;
  diceSides: number;
  diceCount: number | 'initiative';
  iniCalc: 'sum' | 'countHits' | 'none';
  glitch?: 'halfOnes';
  criticalGlitch?: 'noHits';
  multipleInitiativePasses?: boolean;
  copyrightNotice?: string;
  imgsource?: string;
}
