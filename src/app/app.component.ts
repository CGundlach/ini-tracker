// Copyright (c) 2018 Christopher Gundlach

import { Component } from '@angular/core';

import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState
} from '@angular/cdk/layout';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { AboutDialogComponent } from './components/about-dialog/about-dialog.component';

@Component({
  selector: 'initracker-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(
    Breakpoints.Handset
  );
  title = 'initracker';

  constructor(
    private breakpointObserver: BreakpointObserver,
    public dialog: MatDialog
  ) {}

  openAboutDialog() {
    const aboutDialog = this.dialog.open(AboutDialogComponent);
  }
}
