// Copyright (c) 2018 Christopher Gundlach

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'initracker-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
