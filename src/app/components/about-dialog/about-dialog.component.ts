// Initiative Tracker
//
// Copyright 2018 Christopher Gundlach

import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { SYSTEMS } from '../../../assets/systems';
@Component({
  selector: 'initracker-about-dialog',
  templateUrl: './about-dialog.component.html',
  styleUrls: ['./about-dialog.component.css']
})
export class AboutDialogComponent implements OnInit {
  changelog: string;
  version: string;
  SYSTEMS = SYSTEMS;

  constructor(
    private http: HttpClient,
    public dialogRef: MatDialogRef<AboutDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.http
      .get('../../../assets/CHANGELOG', { responseType: 'text' })
      .subscribe(response => {
        this.changelog = response;
      });
    this.http
      .get('../../../assets/VERSION', { responseType: 'text' })
      .subscribe(response => (this.version = response));
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
