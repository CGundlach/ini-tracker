// Initiative Tracker
//
// Copyright 2018 Christopher Gundlach

import { Component, Input, isDevMode, OnInit, ViewChild } from '@angular/core';

import { MatTableDataSource } from '@angular/material';
import { Subscription } from 'rxjs';
import { SYSTEMS } from '../../../assets/systems';
import { ISystem } from '../../models/system';
import { UnitService } from './../../services/unit-manager.service';
import { IniListComponent } from './ini-list/ini-list.component';

@Component({
  selector: 'initracker-main',
  templateUrl: './initracker.component.html',
  styleUrls: ['./initracker.component.css']
})
export class IniTrackerComponent implements OnInit {
  @ViewChild(IniListComponent) iniList: IniListComponent;

  system: ISystem;
  systemName: string;

  systems = SYSTEMS;

  constructor(private unitService: UnitService) {}

  ngOnInit() {
    this.system = this.systems.find(item => item.name === 'kein System');
    this.systemName = this.system.name;
  }

  changeSystem(system: ISystem) {
    this.iniList.changeSystem(system);
  }
}
