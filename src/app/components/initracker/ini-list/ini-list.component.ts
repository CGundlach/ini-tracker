// Copyright (c) 2018 Christopher Gundlach

import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { ISystem } from '../../../models/system';
import { UnitService } from '../../../services/unit-manager.service';

@Component({
  selector: 'initracker-ini-list',
  templateUrl: './ini-list.component.html',
  styleUrls: ['./ini-list.component.css']
})
export class IniListComponent implements OnInit, OnDestroy {
  @Input() system: ISystem;

  units$ = this.unitService.getUnitsIniTracker$();
  iniTracker$ = this.unitService.getUnitsIniTracker$();

  subscriptions: Subscription[] = [];

  anyUnitHasIniPassesLeft = true;
  columnsToDisplay = ['unitName', 'unitIni'];

  constructor(private unitService: UnitService) {}

  ngOnInit() {
    this.subscriptions.push(
      this.iniTracker$.subscribe(units => {
        // Checks wether any units still have initiative passes
        let iniPassesLeft = false;
        for (const unit of units) {
          if (unit.initiativePasses > 1) {
            iniPassesLeft = true;
          }
        }
        this.anyUnitHasIniPassesLeft = iniPassesLeft;
      })
    );
  }

  ngOnDestroy() {
    for (const sub of this.subscriptions) {
      sub.unsubscribe();
    }
  }

  rollIni() {
    this.unitService.rollIniforAll(this.system);
  }

  nextIniPass() {
    this.unitService.nextIniPass(this.system);
  }

  // Change the table as needed for the selected system. Also reroll the initiative
  changeSystem(system: ISystem) {
    const columns = ['unitName', 'unitIni'];
    if (system.multipleInitiativePasses) {
      columns.push('unitIniPasses');
    }
    if (system.iniCalc !== 'none') {
      columns.push('unitDice');
    }
    this.columnsToDisplay = columns;
    this.rollIni();
  }
}
