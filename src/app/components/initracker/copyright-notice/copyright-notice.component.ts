// Copyright (c) 2018 Christopher Gundlach

import { Component, Input, OnInit } from '@angular/core';
import { ISystem } from '../../../models/system';

@Component({
  selector: 'initracker-copyright-notice',
  templateUrl: './copyright-notice.component.html',
  styleUrls: ['./copyright-notice.component.css']
})
export class CopyrightNoticeComponent implements OnInit {
  @Input() system: ISystem;

  constructor() {}

  ngOnInit() {}
}
