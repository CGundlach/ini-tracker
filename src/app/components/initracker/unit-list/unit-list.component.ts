// Copyright (c) 2018 Christopher Gundlach

import { Component, Input, isDevMode, OnInit } from '@angular/core';

import { ISystem } from '../../../models/system';
import { UnitService } from '../../../services/unit-manager.service';

@Component({
  selector: 'initracker-unit-list',
  templateUrl: './unit-list.component.html',
  styleUrls: ['./unit-list.component.css']
})
export class UnitListComponent implements OnInit {
  @Input() system: ISystem;

  units$ = this.unitService.getUnits$();

  constructor(private unitService: UnitService) {}

  ngOnInit() {
    if (isDevMode()) {
      this.addUnit('test');
      this.addUnit('test');
      this.addUnit('test');
      this.addUnit('test');
    }
  }

  addUnit(name: string) {
    this.unitService.addUnit(name, 0);
    if (this.system.iniCalc === 'none') {
      this.unitService.rollIniforAll(this.system);
    }
  }

  // Deaktiviert, bis ich vernünftigen Code gebastelt habe, um "Name-1-1" zu vermeiden
  // duplicateUnit(name: string, initiative?: number) {
  //   this.unitManagerService.addUnit(name, initiative);
  // }

  deleteUnit(name: string) {
    this.unitService.removeUnit(name);
    if (this.system.iniCalc === 'none') {
      this.unitService.rollIniforAll(this.system);
    }
  }

  // Sets the initiative property of a unit
  // For systems without initiative calculation, the unit is added to the initiative tracker and sorted.
  setInitiative(name: string, initiative: number) {
    this.unitService.setInitiative(name, initiative);
    if (this.system.iniCalc === 'none') {
      this.unitService.rollIniforAll(this.system);
    }
  }

  // Sets the initiative passes of a unit
  // For systems without initiative calculation, the unit is added to the initiative tracker and sorted.
  setInitiativePasses(name: string, iniPasses: number) {
    this.unitService.setInitiativePasses(name, iniPasses);
    if (this.system.iniCalc === 'none') {
      this.unitService.rollIniforAll(this.system);
    }
  }
}
