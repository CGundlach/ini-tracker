// Copyright (c) 2018 Christopher Gundlach

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'initracker-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
