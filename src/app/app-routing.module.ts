// Copyright (c) 2018 Christopher Gundlach

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IniTrackerComponent } from './components/initracker/initracker.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: IniTrackerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

export const routedComponents = [];
