// Copyright (c) 2018 Christopher Gundlach

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SYSTEMS } from '../../assets/systems';
import { ISystem } from '../models/system';

@Injectable()
export class UnitService {
  units: IUnit[];
  units$ = new BehaviorSubject<IUnit[]>([]);

  unitsIniTracker: IUnitTracked[];
  unitsIniTracker$ = new BehaviorSubject<IUnitTracked[]>([]);

  constructor() {
    this.units = [];
    this.unitsIniTracker = [];
  }

  public getUnitsIniTracker$(): BehaviorSubject<IUnitTracked[]> {
    return this.unitsIniTracker$;
  }

  public getUnits$(): BehaviorSubject<IUnit[]> {
    return this.units$;
  }

  public setInitiative(name: string, initiative: number) {
    const unitIndex = this.units.findIndex(item => item.name === name);
    this.units[unitIndex].initiative = initiative;
    this.units$.next(this.units);
  }

  public setInitiativePasses(name: string, initiativePasses: number) {
    const unitIndex = this.units.findIndex(item => item.name === name);
    this.units[unitIndex].initiativePasses = initiativePasses;
    this.units$.next(this.units);
  }
  // Adds a new unit to all lists. Checks if the name is already taken and assigns a new one if that's the case
  public addUnit(name: string, initiative?: number) {
    if (!initiative) {
      initiative = 0;
    }
    name = this.getUnusedName(name);
    this.units.push({ name, initiative, initiativePasses: 1 });
    this.units$.next(this.units);
    this.unitsIniTracker.push({
      name,
      initiative,
      rolledIni: 0,
      rolledDice: [],
      glitch: false,
      criticalGlitch: false,
      initiativePasses: 1
    });
  }

  public removeUnit(name: string) {
    const unitIndex = this.units.findIndex(item => item.name === name);
    this.units.splice(unitIndex, 1);
    this.units$.next(this.units);
    const trackerIndex = this.unitsIniTracker.findIndex(
      item => item.name === name
    );
    this.unitsIniTracker.splice(trackerIndex, 1);
    this.unitsIniTracker$.next(this.unitsIniTracker);
  }

  // Reduces the initiative passes of all units by 1, as long as a unit has at least one initiative pass.
  public nextIniPass(system: ISystem) {
    for (const unit of this.unitsIniTracker) {
      if (unit.initiativePasses > 0) {
        unit.initiativePasses--;
      }
    }
    for (const unit of this.unitsIniTracker) {
      if (unit.initiativePasses === 0) {
        this.unitsIniTracker.push(
          this.unitsIniTracker.splice(this.unitsIniTracker.indexOf(unit), 1)[0]
        );
      }
    }
    this.unitsIniTracker$.next(this.unitsIniTracker);
  }

  public rollIniforAll(system: ISystem) {
    for (const unit of this.units) {
      const rolledUnit = this.rollIniForUnit(unit, system);
      const index = this.unitsIniTracker.findIndex(
        item => item.name === unit.name
      );
      this.unitsIniTracker.splice(index, 1, rolledUnit);
    }
    this.unitsIniTracker.sort(this.compareIni);
    this.unitsIniTracker$.next(this.unitsIniTracker);
  }

  private rollIniForUnit(unit: IUnit, system: ISystem): IUnitTracked {
    const unitTracked: IUnitTracked = {
      name: unit.name,
      initiative: unit.initiative,
      rolledIni: 0,
      initiativePasses: 1
    };

    // roll dice according to system
    if (system.diceCount === 'initiative') {
      unitTracked.rolledDice = this.rollDice(unit.initiative, system.diceSides);
    } else {
      unitTracked.rolledDice = this.rollDice(
        system.diceCount,
        system.diceSides
      );
    }

    // initiative calculation
    switch (system.iniCalc) {
      case 'countHits':
        unitTracked.rolledIni =
          +unit.initiative + +this.countHits(unitTracked.rolledDice);
        break;
      case 'sum':
        unitTracked.rolledIni = +unit.initiative;
        for (const die of unitTracked.rolledDice) {
          unitTracked.rolledIni += +die;
        }
        break;
      case 'none':
        unitTracked.rolledIni = +unit.initiative;
        break;
    }

    // set initiative passes
    if (system.multipleInitiativePasses) {
      unitTracked.initiativePasses = unit.initiativePasses;
    }

    // Check for glitches
    switch (system.glitch) {
      case 'halfOnes':
        if (unitTracked.rolledDice.length <= 0) {
          break;
        }
        if (this.countOnes(unitTracked.rolledDice) >= unit.initiative / 2) {
          unitTracked.glitch = true;
          unitTracked.rolledIni = 0;
        }
        break;
    }

    // Check for critical glitches
    switch (system.criticalGlitch) {
      case 'noHits':
        if (
          unitTracked.glitch &&
          this.countHits(unitTracked.rolledDice) === 0
        ) {
          unitTracked.criticalGlitch = true;
          if (unitTracked.initiativePasses > 1) {
            unitTracked.initiativePasses--;
          }
        }
        break;
    }

    return unitTracked;
  }

  private diceRollTest(sides: number) {
    const rolledDice = this.rollDice(10000, sides);
    const rollCount = new Array<number>();
    for (let i = 0; i <= sides; i++) {
      rollCount.push(0);
    }
    for (const die of rolledDice) {
      rollCount[die]++;
    }
    console.log(rollCount);
  }

  // Rolls the given amount of dice with the given amount of sides. Returns an array with the results.
  private rollDice(amount: number, sides: number): number[] {
    const rollStorage = [];
    for (let i = 1; i <= amount; i++) {
      rollStorage.push(Math.floor(Math.random() * sides) + 1);
    }
    return rollStorage;
  }

  // Counts how many dice show a 5 or 6
  private countHits(rolledDice: number[]): number {
    let hitCount = 0;
    for (const roll of rolledDice) {
      if (roll === 5 || roll === 6) {
        hitCount++;
      }
    }
    return hitCount;
  }

  // Counts how many dice are showing a 1
  private countOnes(rolledDice: number[]): number {
    let oneCount = 0;
    for (const roll of rolledDice) {
      if (roll === 1) {
        oneCount++;
      }
    }
    return oneCount;
  }

  private compareIni(a, b) {
    if (a.rolledIni > b.rolledIni) {
      return -1;
    }
    if (a.rolledIni < b.rolledIni) {
      return 1;
    }
    return 0;
  }

  // Checks if the given name is already taken. If yes, a hyphen and digit is appended.
  // Is that name already taken, too, the digit will be increases by 1 up to a thousand times until a free name is found.
  // Returns the first name that is not in use. Or the original name, if there are already 1000 instances of it. Fixing that later.
  private getUnusedName(desiredName: string): string {
    if (this.units.find(unit => unit.name === desiredName)) {
      for (let i = 1; i <= 1000; i++) {
        if (!this.units.find(unit => unit.name === desiredName + '-' + i)) {
          return desiredName + '-' + i;
        }
      }
    }
    return desiredName;
  }
}

interface IUnit {
  name: string;
  initiative: number;
  initiativePasses: number;
}

interface IUnitTracked extends IUnit {
  rolledIni: number;
  rolledDice?: number[];
  glitch?: boolean;
  criticalGlitch?: boolean;
}
