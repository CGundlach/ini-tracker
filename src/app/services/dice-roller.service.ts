// Copyright (c) 2018 Christopher Gundlach

import { Injectable } from '@angular/core';

@Injectable()
export class DiceRollerService {
  constructor() {}

  rollDice(amount: number, sides: number): string[] {
    const rollStorage = [];
    for (let i = 1; i <= amount; i++) {
      rollStorage.push(Math.floor(Math.random() * sides) + 1);
    }
    return rollStorage;
  }
}
