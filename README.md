# InitiativeTracker

The goal of this project is to provide a system-independent initiative tracker for pen&paper role playing games, as well as provide modules to directly roll initiative in various game systems.

If you would like additional systems, please provide them with an explanation of the initiative system in the issue tracker.
You can also take a loot at the contribution guidelines to add the system yourself.

The current build of the `master` branch is currently hosted on https://initracker.kalyndor.de.

## Getting started

### Prerequisites

Install instructions are provided on the pages linked here.

- [Git](https://git-scm.com/)
- [Node.js](https://nodejs.org/en/) version 8
- [Angular CLI](https://github.com/angular/angular-cli) version 6
- [Python](https://www.python.org/) version 2.7 or later (Deployment only)

### Installing

Clone this repository

```
git clone git@gitlab.com:CGundlach/iniTracker.git
```

Open a terminal (e.g. bash, cmd, powershell) and install the dependencies via npm

```
npm install
```

Done! You're ready to start the development server.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Tests

Tests are currently not implemented. Once they are, you can run unit tests with `ng test` and e2e tests with `ng e2e`.  
This section will be updated once tests are implemented.

### Static Code Analysis

This projects makes use of [tslint](https://palantir.github.io/tslint/) and [prettier](https://prettier.io/). Configuration files are included in the repository. You can check if your code is formatted properly by running `ng lint`

### Deploy

Run `python ./bin/deploy.py -h <host>:<port> -u <FTPuser> -p <password>` to deploy the built project to the root directory of a ftp user on your server.

## Built With

- [Angular v6](https://angular.io/) - Framework
- [npm](https://www.npmjs.com/) - Dependency Management
- [Angular Material v6](https://material.angular.io) - Theme

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/CGundlach/iniTracker/blob/master/CONTRIBUTING.md) for details.

## Authors

- Christopher Gundlach - initial work - @CGundlach

## License

See the LICENSE.md file.

## Acknowledgments

Thanks to my pen & paper role playing groups who were the main reason I started writing this app, and who also give me feedback on what to improve.
Also many thanks to everyone answering Questions over on [StackOverflow](https://stackoverflow.com/) and [reddit](https://reddit.com), who were elemental in me getting a better understanding of Angular, Typescript and CSS over the course of this and previous projects.
